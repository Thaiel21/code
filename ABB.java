package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    private List<T> elementosEliminados = new ArrayList<>();
    private class Nodo {
        T valor;
        Nodo izq;
        Nodo der;
        Nodo padre;

        Nodo (T v){
          valor = v;
          izq = null;
          der = null;
          padre = null;
        }
    }
        
    private Nodo _raiz;
    private int cardinal;

    public ABB() {
        _raiz = null;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        Nodo actual = _raiz;
        while(actual.izq != null){
            actual = actual.izq; 
        }
        return actual.valor;
    }

    public T maximo(){
        Nodo actual = _raiz;
        while(actual.der != null){
            actual = actual.der; 
        }
        return actual.valor;
    }

    public void insertar(T elem){
        Nodo nuevo = new Nodo(elem);
        Nodo actual = _raiz;
        if(_raiz == null){
            _raiz = nuevo;
            cardinal++;
        }else{
            while(actual != null){
                if(actual.valor.compareTo(elem) > 0){
                    if(actual.izq == null){
                        actual.izq = nuevo;
                        cardinal++;
                    }else{
                        actual = actual.izq;
                    }
                }else if(actual.valor.compareTo(elem) < 0){
                    if(actual.der == null){
                    actual.der = nuevo;
                    cardinal++;                        
                    }else{
                        actual = actual.der;
                    }
                }else{
                    return;
                }
            }           
        }
    }

    public boolean pertenece(T elem){
        Nodo actual = _raiz;
        while(actual != null){
            if(actual.valor.compareTo(elem) == 0){
               return true; 
            }else if(actual.valor.compareTo(elem) > 0){
                actual = actual.izq;
            }else {
                actual = actual.der;                
            }
        }
        return false;        
    }

    public void eliminar(T elem) {
        Nodo actual = _raiz;
        while (actual != null) {
            if (actual.valor.compareTo(elem) == 0) {
                if (actual.izq == null && actual.der == null) {
                    if(actual.padre == null){
                    }else{
                        actual = null;
                    }                    
                }else if (actual.izq == null) {
                    if (actual.padre == null) {
                    }else{
                        actual.der.padre = actual.padre;
                        actual = null;
                    }
                }else if (actual.der == null){
                        if(actual.padre == null){
                        }else {
                        actual.izq.padre = actual.padre;
                        actual = null;
                        }
                }else{
                    if(actual.der.izq == null){
                        actual.der.padre = actual.padre;
                        actual = null;
                    }else{
                        actual.der.izq.padre = actual.padre;
                        actual.der.padre = actual.der.izq;
                        actual = null;
                    }
                }
                cardinal--;
                elementosEliminados.add(elem);
                break;
            }else if (elem.compareTo(actual.valor) < 0) {
                actual = actual.izq;
            }else {
                actual = actual.der;
            }
        }
    }
        
    public String toString() {
        Stack<Nodo> pila = new Stack<>();
        Nodo actual = _raiz;
        boolean first = true;
        StringBuilder sb = new StringBuilder("{");

        while (!pila.isEmpty() || actual != null) {
            while (actual != null) {
                pila.push(actual);
                actual = actual.izq;
            }
            if (!pila.isEmpty()) {
                actual = pila.pop();

                if (!elementosEliminados.contains(actual.valor)) {
                    if (!first) {
                        sb.append(",");
                    } else {
                        first = false;
                    }
                    sb.append(actual.valor);
                }
                actual = actual.der;
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    
     
    private class ABB_Iterador implements Iterador<T> {
        private Nodo actual;

        public boolean haySiguiente() {        
            if (actual.der == null){
                return false;
            }    
            return true;
        }
    
        public T siguiente() {
            return this.actual.valor;
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
